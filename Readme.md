# Templates for nix developer environments for [devenv.sh](https://devenv.sh/)

### Overview:
- [How to use](#how-to-use)
- [List of templates below](#templates)
- [Tips & Tricks](#tips-tricks)


## How to use

### Prerequisites
- Install [Nix package manager](https://nixos.org/download.html)
- Enable [flake support](https://nixos.wiki/wiki/Flakes)
- (optional, but recommended) Install [direnv](https://direnv.net/)

**tip: try variant C**

### variant A: nix template
This is the simple & 'usual' way of initialising a repo with a template (works also for existing repos) - which works great, but does not have a good worflow for merging & keeping up-to-date with the boilerplate.

```console
nix flake init -t git+https://gitlab.com/txlab/dx/devenv-templates.git/#js
```
Note: Because of [nix#6435](https://github.com/NixOS/nix/issues/6435) we can't use `gitlab:`

### variant B: git repo
This is an experimental way of using the template as a git remote, and merging the template into your repo. This enabled a few benefits:
- use the tools you're used to for merging
- have a clearly defined commit history & origin for the template
- in case of template updates, you can merge it into your repo easily and using well-known workflows (instead of having repos with stale boilerplate)

```console
git remote add devenv-template https://gitlab.com/txlab/dx/templates/basic.git && git fetch devenv-template
git merge --allow-unrelated-histories devenv-template/main # optional: --squash
```

### Variant C: devenvious
Because I like the idea of B, but not the effort, I've created [devenvious](https://gitlab.com/txlab/dx/devenvious):
```shell
dvnv init js
```

## Templates

[You can find explore template contents as separate repos here](https://gitlab.com/txlab/dx/templates) (e.g. for use with devenvious)

### [Basic](https://gitlab.com/txlab/dx/templates/basic)
minimal devshell & direnv setup
```console
nix flake init -t gitlab:txlab%2fdx/devenv-templates
```
- variant: [with taskfile](https://gitlab.com/txlab/dx/templates/basic-withtask)

### [Javascript / Typescript](https://gitlab.com/txlab/dx/templates/js)
NodeJS LTS & yarn
```console
nix flake init -t gitlab:txlab%2fdx/devenv-templates#node
```

### [Rust](https://gitlab.com/txlab/dx/templates/rust)
Rust (using oxalica overlay & crane for building)
```console
nix flake init -t gitlab:txlab%2fdx/devenv-templates#rust
```

## Tips & Tricks

### Nix-portable (~ polyfill)
If you work with devs that don't want to install nix. 😋
See https://gitlab.com/txlab/tools/nix-portable-direnv
