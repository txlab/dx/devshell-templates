{
  description = "A collection of templates to bootstrap project-local developer environments";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05"; # or use /nixos-unstable to get latest packages, but maybe less caching
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable"; # or use /nixos-unstable to get latest packages, but maybe less caching
    devenv.url = "github:cachix/devenv";
    systems.url = "github:nix-systems/default"; # (i) allows overriding systems easily, see https://github.com/nix-systems/nix-systems#consumer-usage
  };

  outputs = inputs@{ self, flake-parts, nixpkgs, nixpkgs-unstable, systems, devenv, ... }: (
    flake-parts.lib.mkFlake { inherit inputs; } {

      flake = (import ./templates.nix);

      imports = [
        inputs.devenv.flakeModule
      ];
      systems = (import systems);
      perSystem = { config, self', inputs', pkgs, system, ... }: # perSystem docs: https://flake.parts/module-arguments.html#persystem-module-parameters
        let
          pkgs = nixpkgs.legacyPackages.${system};
          pkgs-unstable = nixpkgs-unstable.legacyPackages.${system};
        in
        {
          devenv.shells.default = (import ./devenv.nix {
            inherit pkgs inputs;
            latest = pkgs-unstable;
          });
        };
    }
  );

  nixConfig = {
    extra-substituters = [ "https://devenv.cachix.org" ];
    extra-trusted-public-keys = [ "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw=" ];
  };
}
