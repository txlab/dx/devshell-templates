#!/usr/bin/env bash
set -euo pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd ) # https://stackoverflow.com/a/246128/1633985

for dir in */; do
    template=${dir%/}
    echo "########################"
    echo "## TESTING: $template"
    echo "########################"
    cd "$(mktemp -d)"
    
    nix flake init -t "$SCRIPT_DIR#$template"
    direnv allow
    direnv exec . echo Hello from inside $template
    # nix develop -c echo Hello shell

    echo
done