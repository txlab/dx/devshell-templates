rec {

  templates = {

    basic = {
      path = ./basic;
      description = "Basic devenv";
      welcomeText = ''
        You just initialized your project with a devenv.  
        Run `direnv allow` to enter it.

        Check out `./devenv.nix` for more details on how to customize it.
      '';
    };

    basic-withtask = {
      path = ./basic-withtask;
      description = "Basic devenv & Taskfile setup";
      welcomeText = ''
        You just initialized your project with a devenv & Taskfile setup.  
        Run `direnv allow` to enter devenv.

        Check out `./Taskfile.yml`, `./devenv.nix` for more details on how to customize it.
      '';
    };

    js = {
      path = ./js;
      description = "A devenv with NodeJS";
      welcomeText = ''
        You just initialized your project with a devenv.  
        Run `direnv allow` to enter it.  

        Check out `./devenv.nix` for more details on how to customize it.
      '';
    };

    rust = {
      path = ./rust;
      description = "A devenv with Rust toolkit";
      welcomeText = ''
        You just initialized your project with a devenv.  
        Run `direnv allow` to enter it.  

        Check out `./devenv.nix` for more details on how to customize it.
      '';
    };

  };

  defaultTemplate = templates.basic;
}
